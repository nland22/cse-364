\documentclass{article}

\usepackage{fancyhdr}
\usepackage{extramarks}
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{amsfonts}
\usepackage{tikz}
\usepackage[plain]{algorithm}
\usepackage{algpseudocode}

\usetikzlibrary{shapes,arrows,positioning,calc}

%
% Basic Document Settings
%

\topmargin=-0.45in
\evensidemargin=0in
\oddsidemargin=0in
\textwidth=6.5in
\textheight=9.0in
\headsep=0.25in

\linespread{1.1}

\pagestyle{fancy}
\lhead{\hmwkAuthorName}
\chead{\hmwkClass : \hmwkTitle}
\rhead{\firstxmark}
\lfoot{\lastxmark}
\cfoot{\thepage}

\renewcommand\headrulewidth{0.4pt}
\renewcommand\footrulewidth{0.4pt}

\setlength\parindent{0pt}

%
% Create Problem Sections
%

\newcommand{\enterProblemHeader}[1]{ \nobreak\extramarks{}{Problem
    \arabic{#1} continued on next page\ldots}\nobreak{}
  \nobreak\extramarks{Problem \arabic{#1} (continued)}{Problem
    \arabic{#1} continued on next page\ldots}\nobreak{} }

\newcommand{\exitProblemHeader}[1]{ \nobreak\extramarks{Problem
    \arabic{#1} (continued)}{Problem \arabic{#1} continued on next
    page\ldots}\nobreak{} \stepcounter{#1} \nobreak\extramarks{Problem
    \arabic{#1}}{}\nobreak{} }

\setcounter{secnumdepth}{0} \newcounter{partCounter}
\newcounter{homeworkProblemCounter}
\setcounter{homeworkProblemCounter}{1} \nobreak\extramarks{Problem
  \arabic{homeworkProblemCounter}}{}\nobreak{}

%
% Homework Problem Environment
%
% This environment takes an optional argument. When given, it will adjust the
% problem counter. This is useful for when the problems given for your
% assignment aren't sequential. See the last 3 problems of this template for an
% example.
%
\newenvironment{homeworkProblem}[1][-1]{ \ifnum#1>0
  \setcounter{homeworkProblemCounter}{#1} \fi
  \section{Problem \arabic{homeworkProblemCounter}}
  \setcounter{partCounter}{1}
  \enterProblemHeader{homeworkProblemCounter} }{
  \exitProblemHeader{homeworkProblemCounter} }

%
% Homework Details
%   - Title
%   - Due date
%   - Class
%   - Section/Time
%   - Instructor
%   - Author
%

\newcommand{\hmwkTitle}{Final Exam Notes}
\newcommand{\hmwkDueDate}{}
\newcommand{\hmwkClass}{Computer Organization}
\newcommand{\hmwkClassTime}{Friday 8:00--11:47}
\newcommand{\hmwkClassInstructor}{Professor Debatosh Debnath}
\newcommand{\hmwkAuthorName}{Nicholas Land}

%
% Title Page
%

\title{
  \vspace{2in}
  \textmd{\textbf{\hmwkClass:\ \hmwkTitle}}\\
  \vspace{0.1in}\large{\textit{\hmwkClassInstructor\ \hmwkClassTime}}
  \vspace{3in}
}

\author{\textbf{\hmwkAuthorName}} \date{}

\renewcommand{\part}[1]{\textbf{\large Part
    \Alph{partCounter}}\stepcounter{partCounter}\\}

%
% Various Helper Commands
%

% Useful for algorithms
\newcommand{\alg}[1]{\textsc{\bfseries \footnotesize #1}}

% For derivatives
\newcommand{\deriv}[1]{\frac{\mathrm{d}}{\mathrm{d}x} (#1)}

% For partial derivatives
\newcommand{\pderiv}[2]{\frac{\partial}{\partial #1} (#2)}

% Integral dx
\newcommand{\dx}{\mathrm{d}x}

% Alias for the Solution section header
\newcommand{\solution}{\textbf{\large Solution}}

% Probability commands: Expectation, Variance, Covariance, Bias
\newcommand{\E}{\mathrm{E}} \newcommand{\Var}{\mathrm{Var}}
\newcommand{\Cov}{\mathrm{Cov}} \newcommand{\Bias}{\mathrm{Bias}}

\begin{document}

\maketitle

\pagebreak

\section{Memory Organization}

There are two types of memory \textbf{Read Only Memory (ROM)} \&
\textbf{Random Access Memory (RAM)}. \\

\textbf{ROM}: \hspace{.1cm} Read Only Memory is designed for applications in which
data is only read. This type of chip is programmed with data before
they are added to the computer. Once this is done, the data usually
does not change. This chip always retains it's data, even when there
is no power. An Example would be a microwave. A microwave contains one
program, and it doesn't change regardless of what happens to the
power. \\

There are a few different types of \textbf{ROM} chips:
\begin{enumerable}
  \item \textbf{PROM}: \hspace{.1cm}
    \textbf{Programmable ROM}: This is a type of chip that can be
    programmed by a user with any standard PROM programmer.
  \item \textbf{EPROM}: \hspace{.1cm}
    \textbf{Erasable PROM}: This is much like PROM, but it can also
    be erased. There are charged and uncharged capacitors that cause
    each word of memory to store the correct value. It is erased with
    ultraviolet light.
  \item \textbf{EEPROM}: \hspace{.1cm}
    \textbf{Electronically Erasable PROM}: Sometimes denoted
    \textbf{E\textsuperscript{2}PROM}. It works like EPROM, but the contents are erased
    electronically. Unlike EPROM, some of the contents can remain
    unchanged when erasing this chip.
  \item \textbf{Flash EEPROM}: \hspace{.1cm}
    Erases blocks of data, rather than individual locations.
\end{enumerable}

\textbf{RAM}: \hspace{.1cm} Random Access Memory, or \textit{Read/Write} memory can
be used to store data that changes. RAM loses it's data if the power
is cut. The obvious example for RAM is computer chips inside of
computers. \\

There are a few different types of \textbf{RAM} chips:
\begin{enumerable}
  \item \textbf{Dynamic RAM}: \hspace{.1cm}
    Dynamic RAM or DRAM are like leaky capacitors. The charge slowly
    leaks out and eventually the charge would be too low to represent
    valid data. However, \textbf{refresh} circuitry reads the content
    of the DRAM and rewrites data to the original locations.
  \item \textbf{Static RAM}: \hspace{.1cm}
    Static RAM or SRAM is more like a register than a leaky capacitor.
    Once data is written to SRAM it's content stay valid and does not
    have to be refreshed. Static RAM is faster than DRAM, but it is
    also more expensive. The cache memory in PC's are constructed from
    SRAM.
\end{enumerable}

\tikzstyle{block} = [rectangle, draw, text width=5em, text centered,
  minimum height=4em]

\begin{tikzpicture}
  \node [block] (roma) {ROM};
  \node [block, below =2cm of roma] (romb) {ROM};
\end{tikzpicture}

\pagebreak

\section*{5.1}
\section*{5.2}
\section*{5.3 Up to page 394}
\section*{5.7 Up to page 440}
\section*{RISC Rationale}
\section*{Storage \& Other I/O topics}

Average Rotational Latency =

\begin{equation*}
  \frac{0.5}{\text{RPM} / {{60 \frac{Seconds}{Minute}}}}
\end{equation*}\\

For example: If a disk is $15,000$ RPM, then the Average Rotational
latency is:

\begin{equation*}
  \frac{0.5}{\text{15,000} / {{60 \frac{Seconds}{Minute}}}} = 0.002
  \text{ seconds}
\end{equation*}\\

Which translates to $2.0$ ms (milliseconds)\\

Then \textit{Disk Read Time} is equal to \textit{average seek time} +
\textit{average rotational latency} + \textit{transfer time} +
\textit{controller overhead}

\section*{Multiprocessors}
\section*{Power Wall}
\section*{Wallace Tree}


\end{document}